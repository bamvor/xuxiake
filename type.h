#ifndef __TYPE_H
#define __TYPE_H
typedef unsigned long	u64;
typedef long		s64;
typedef unsigned int	u32;
typedef int		s32;
typedef unsigned char	u8;
typedef signed char	s8;

#define NULL		((void*)0)

#endif /* #ifdef __TYPE_H */
